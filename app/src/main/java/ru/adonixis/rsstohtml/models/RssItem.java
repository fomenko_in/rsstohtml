package ru.adonixis.rsstohtml.models;


public class RssItem {

    private String guid;
    private String title;
    private String link;
    private String description;
    private String pubDate;
    private String category;
    private String yandexFullText;
    private String ramblerFullText;

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setYandexFullText(String yandexFullText) {
        this.yandexFullText = yandexFullText;
    }

    public void setRamblerFullText(String ramblerFullText) {
        this.ramblerFullText = ramblerFullText;
    }


    public String getGuid() {
        return guid;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public String getCategory() {
        return category;
    }

    public String getYandexFullText() {
        return yandexFullText;
    }

    public String getRamblerFullText() {
        return ramblerFullText;
    }

}
