package ru.adonixis.rsstohtml;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ru.adonixis.rsstohtml.models.RssItem;

public class MainActivity extends AppCompatActivity {

    WebView webView;
    private final static String RSS_URL = "http://feeds.feedburner.com/sgpress/dIKC";
    private enum RSSXMLTag {
        TITLE, DESCRIPTION, PUB_DATE, FULL_TEXT, IGNORETAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        webView = (WebView) findViewById(R.id.webView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startLoading();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            startLoading();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startLoading() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            RssDataController rssDataController = new RssDataController(this);
            rssDataController.execute(RSS_URL);
        } else {
            webView.loadData("Check network connection", "text/html; charset=utf-8", "utf-8");
        }

    }

    private RssItem xmlParse(Reader in) {

        RssItem rssItem = null;
        RSSXMLTag currentTag = null;

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(in);

            int eventType = xpp.getEventType();
            SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss", Locale.US);
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {

                } else if (eventType == XmlPullParser.START_TAG) {
                    if (xpp.getName().equals("item")) {
                        rssItem = new RssItem();
                        currentTag = RSSXMLTag.IGNORETAG;
                    } else if (xpp.getName().equals("title")) {
                        currentTag = RSSXMLTag.TITLE;
                    } else if (xpp.getName().equals("description")) {
                        currentTag = RSSXMLTag.DESCRIPTION;
                    } else if (xpp.getName().equals("pubDate")) {
                        currentTag = RSSXMLTag.PUB_DATE;
                    } else if (xpp.getName().equals("full-text")) {
                        currentTag = RSSXMLTag.FULL_TEXT;
                    }
                } else if (eventType == XmlPullParser.END_TAG) {
                    if (xpp.getName().equals("item")) {
                        try {
                            if (rssItem.getPubDate() != null) {
                                Date postDate = dateFormat.parse(rssItem.getPubDate());
                                SimpleDateFormat resultDateFormat = new SimpleDateFormat("HH:mm dd/MM/yyyy", Locale.US);
                                rssItem.setPubDate(resultDateFormat.format(postDate));
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        break;
                    } else {
                        currentTag = RSSXMLTag.IGNORETAG;
                    }
                } else if (eventType == XmlPullParser.TEXT) {
                    String content = xpp.getText();
                    content = content.trim();
                    if (rssItem != null) {
                        switch (currentTag) {
                            case TITLE:
                                if (content.length() != 0) {
                                    if (rssItem.getTitle() != null) {
                                        rssItem.setTitle(rssItem.getTitle() + content);
                                    } else {
                                        rssItem.setTitle(content);
                                    }
                                }
                                break;
                            case DESCRIPTION:
                                if (content.length() != 0) {
                                    if (rssItem.getDescription() != null) {
                                        rssItem.setDescription(rssItem.getDescription() + content);
                                    } else {
                                        rssItem.setDescription(content);
                                    }
                                }
                                break;
                            case PUB_DATE:
                                if (content.length() != 0) {
                                    if (rssItem.getPubDate() != null) {
                                        rssItem.setPubDate(rssItem.getTitle() + content);
                                    } else {
                                        rssItem.setPubDate(content);
                                    }
                                }
                                break;
                            case FULL_TEXT:
                                if (content.length() != 0) {
                                    if (rssItem.getYandexFullText() != null) {
                                        rssItem.setYandexFullText(rssItem.getYandexFullText() + content);
                                    } else {
                                        rssItem.setYandexFullText(content);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                eventType = xpp.next();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }

        return rssItem;
    }

    private String htmlParse(RssItem rssItem) {
        Document docHTML = null;
        InputStream isHTML;

        try {
            isHTML = getAssets().open("index.html");
            docHTML = Jsoup.parse(isHTML, "UTF-8", RSS_URL);
            isHTML.close();

            Document doc = Jsoup.parse(rssItem.getDescription());
            String img = doc.select("img").first().toString();
            String subtitle = doc.text();

            Element title = docHTML.select("title").first();
            title.html(rssItem.getTitle());
            Element h1 = docHTML.select("h1").first();
            h1.html(rssItem.getTitle());
            Element h2 = docHTML.select("h2").first();
            h2.html(subtitle);
            Element h6 = docHTML.select("h6").first();
            h6.html(rssItem.getPubDate());
            Element section = docHTML.select("section.clearfix").first();
            section.append(img);
            Element p = docHTML.select("p").first();
            p.html(rssItem.getYandexFullText());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return docHTML.toString();
    }

    private class RssDataController extends AsyncTask<String, Void, String> {
        private Context mContext;
        private ProgressDialog loadingDialog;

        RssDataController(Context context)
        {
            mContext = context;
            loadingDialog = new ProgressDialog(mContext);
            loadingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            loadingDialog.setMessage("Loading, please wait...");
            loadingDialog.setCancelable(false);
            loadingDialog.setIndeterminate(true);
            loadingDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String url = params[0];
            RssItem rssItem = null;

            Reader in = null;
            Response response;
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(url)
                    .get()
                    .build();
            try {
                response = client.newCall(request).execute();
                in = response.body().charStream();
            } catch (IOException e) {
                e.printStackTrace();
            }

            rssItem = xmlParse(in);
            String html = htmlParse(rssItem);

            return html;
        }

        @Override
        protected void onPostExecute(String result) {
            webView.loadData(result, "text/html; charset=utf-8", "utf-8");
            loadingDialog.dismiss();
            setTitle(webView.getTitle());
            super.onPostExecute(result);
        }
    }
}
